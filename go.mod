module gitlab.com/opennota/widdly

go 1.15

require (
	github.com/aws/aws-sdk-go v1.42.2
	github.com/boltdb/bolt v1.3.1
	github.com/daaku/go.zipexe v1.0.1
	golang.org/x/crypto v0.0.0-20211108221036-ceb1ce70b4fa
	golang.org/x/sys v0.0.0-20211110154304-99a53858aa08 // indirect
)
